import 'package:clima/services/location.dart';
import 'package:clima/services/networking.dart';

const openWeatherMapAPIKey = '312cd0396b77cd929bf083ee0b7da51e';
const openWeatherMapUrl = 'https://api.openweathermap.org/data/2.5/weather';

class WeatherModel {
  Future<dynamic> getCityWeather(String cityName) async {
    Location location = Location();
    await location.getCurrentLocation();
    String uri = '$openWeatherMapUrl?q=$cityName&units=metric&appid=$openWeatherMapAPIKey';
    NetworkService networkService = NetworkService(uri: uri);
    dynamic weatherData = await networkService.getData();
    return weatherData;
  }

  Future<dynamic> getCurrentLocationWeather() async {
    Location location = Location();
    await location.getCurrentLocation();
    String uri =
        '$openWeatherMapUrl?lat=${location.lat}&lon=${location.lng}&units=metric&appid=$openWeatherMapAPIKey';
    NetworkService networkService = NetworkService(uri: uri);
    dynamic weatherData = await networkService.getData();
    return weatherData;
  }

  String getWeatherIcon(int condition) {
    if (condition < 300) {
      return '🌩';
    } else if (condition < 400) {
      return '🌧';
    } else if (condition < 600) {
      return '☔️';
    } else if (condition < 700) {
      return '☃️';
    } else if (condition < 800) {
      return '🌫';
    } else if (condition == 800) {
      return '☀️';
    } else if (condition <= 804) {
      return '☁️';
    } else {
      return '🤷‍';
    }
  }

  String getMessage(int temp) {
    if (temp > 25) {
      return 'It\'s 🍦 time';
    } else if (temp > 20) {
      return 'Time for shorts and 👕';
    } else if (temp < 10) {
      return 'You\'ll need 🧣 and 🧤';
    } else {
      return 'Bring a 🧥 just in case';
    }
  }
}
