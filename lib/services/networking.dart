import 'package:http/http.dart' as http;
import 'dart:convert';

class NetworkService {
  NetworkService({this.uri});

  final String uri;

  Future<dynamic> getData() async {
    http.Response response = await http.get(uri);
    if (response.statusCode == 200) {
      final String data = response.body;
      final dynamic decodedJson = json.decode(data);
      return decodedJson;
    } else {
      print(response.statusCode);
    }
  }
}
